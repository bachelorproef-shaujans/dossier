# Sketch bestandsstructuur

Sketchdesigns worden opgebouwd op opgeslaan in de vorm van jsonbestanden. Deze jsonbestanden definieren alle eigenschappen van alle elementen in het design. Een Sketchbestand is een verzameling van jsonbestanden en afbeeldingen zoals hieronder afgebeeld.

```
bestandsnaam
	- document.json
	- meta.json
	- user.json
	- images/
	- pages/
		- <referentie>.json
	- previews/
		- preview.png
```

## Inhoud van mappen en bestanden

### document.json

`document.json` bevat informatie over het volledige Sketchbestand. Enkele belangrijke elementen die dit bestand bevat zijn de verschillende afbeeldingen binnen het design aangeduild met `imageCollection`, de laagstijlen aangeduid met `layerStyles`, de beschikbare symbolen aangeduild met `layerSymbols`, de tekstlaagstijlen aangeduild met `layerTextStyles` en alle pagina's binnen het document aangeduid met `pages`. 

### meta.json

`meta.json` bevat metadata over het volledige document en in welke versie van Sketch dit design is gemaakt. Verder bevat dit document ook nog een verwijzing naar alle pagina's en de artboards inclusief de referentienummers en de gebruikte lettertypes onder de key `fonts`.

### user.json

`user.json` bevat informatie over de huidige weergave van het bestand waaronder de scrollpositie en het zoomniveau. Dit bestand  bevat geen belangrijke data die nodig is om het bestand later te interpreteren.

### images/

Alle afbeeldingen die in het document worden gesleept om te gebruiken in het design worden in de map `images` geplaatst.

### pages/

In de map `pages` zitten alle pagina's binnen een Sketchbestand. Deze worden gelabeled met een uniek ID dat ook in de documenten `document.json` en `meta.json` terugkomen. 

### pages/<referentie>.json

Dit bestand bevat alle informatie over de pagina en is veelal een groot bestand met een diepe nesting van arrays en objecten. Doch zit er een terugkomende structuur in waardoor je relatief gemakkelijk het bestand kan ontcijferen. Op het eerste niveau van het object vind je de pagina. Dit kan je afleiden door de key `<class>`, deze heeft de waarde `MSPage`. Later in het document vind je ook een lijst met alle mogelijke types. Een niveau in dit bestand ziet er als volgt uit.

```
isFlippedHorizontal: 0|1
style: {}
horizontalRulerData: {}
frame: {}
hasClickThrough: 0|1
includeInCloudUpload: 0|1
exportOptions: {}
objectID: string
rotation: number
layerListExpandedType: 0|1
verticalRulerData: {}
isFlippedVertical: 0|1
resizingType: 0|1
<class>: string
layers: {}
isVisible: 0|1
nameIsFixed: 0|1
name: string
isLocked: 0|1
shouldBreakMaskChain: 0|1
resizingConstraint: number
```

Enkele belangrijke elementen in bovenstaand object zijn `style` dat de stijlen van het element bepaald waaronder de achtergrondkleur, borders, schaduwen, etc. `frame` bevat de hoogte en breedte van het element samen met de X- en Y-waarden van het element relatief van het parentelement. `<class>` bevat het type van de laag, `layers` bevat alle lagen die onder dit niveau komen en bovenstaande structuur zal dan ook terugkomen, samen met bepaalde andere keys. Als laatste bevat `name` de naam van de laag.

### previews/preview.png

Bij het opslaan van een bestand wordt er een bestand `preview.png` gemaakt met daarin alle artboards in afgebeeld. Dit bestand wordt onder andere gebruikt om snelle previews weer te geven van het Sketchdesign in finder op Mac. 

## Verschillende laagtypes

Elke laag wordt gelabeled met `<class>` waarmee het type van de laag wordt meegegeven. Hieronder een lijst van verschillende types en extra uitleg bij het type.

* MSPage: pagina
* MSArtboardGroup: artboard
* MSLayerGroup: groep met lagen
* MSShapeGroup: groep met shapes
  * MSRectangleShape: vierhoek
  * MSOvalShape: ovaal
  * MSStarShape: ster
  * MSTriangleShape: driehoek
  * MSShapePathLayer: vector
* MSTexLayer: tekstlaag
* MSBitmapLayer: afbeelding
  * image: verwijzing naar afbeelding in `images/` map

### Verschillende stijlen

Elke laag bevat stijlen, deze zijn gedefinieerd in het object `style` en bevatten onder andere de achtergrondkleur, borders, schaduwen en meer. Hieronder een lijst met de meestvoorkomende stijlen.

* fills: array van alle achtergrondkleuren, -overgangen en -afbeeldingen.
  * filltype: type van de vulkleur
    * 0: solide volkleur
    * 1: gradient
    * ?: 
  * isEnabled: laag geactiveerd of niet
  * color: solide vulkleur
  * gradient:
    * from / to: waar het gradient vertrekt en waar het stopt in de vorm van x/y-coordinaten
    * stops: array van punten inclusief overeenstemmende kleur
* border: array van alle borders
  * filltype: type van border vulkleur
  * thickness: breedte van de border
  * position:
    * ?: center?
    * 1: inset
    * 2: outside
  * color
  * gradient
* shadows / innerShadows
  * offsetX
  * offsetY
  * blurRadius
  * spread
  * color